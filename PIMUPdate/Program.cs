﻿using System;
using System.IO;
using OfficeOpenXml;


namespace PIMUPdate
{
    class Program
    {

        private static ExcelWorksheet p_sheet, c_sheet;

        private static bool[] tagsbool = new bool[32];

        private static int p_r = 4;
        private static int c_tagsstart = 18, tagsNo = 32, p_tagsstart = 118, p_dtype = 151, p_namerow = 103;

        private static ExcelPackage p_infile;

        private static int FindWorksheet(ExcelPackage infile, string worksheetName)
        {
            var worksheetCounts = infile.Workbook.Worksheets.Count;
            var found = false;
            int i = 1;

            while (!found && i <= worksheetCounts)
            {
                if (infile.Workbook.Worksheets[i].Name.ToUpper() == worksheetName.ToUpper())
                {
                    found = true;
                }
                else
                {
                    i += 1;
                }
            }

            if (!found)
            {
                Console.WriteLine($"Cannot find worksheet {worksheetName}");
                i = 0;
            }
            return i;
        }

        private static void checkTags(int row)

        {
            string x = "";

            for (var a = 0; a < tagsNo; a++)
            {
                if (c_sheet.Cells[row, c_tagsstart + a].Value != null && c_sheet.Cells[row, c_tagsstart + a].Text == "1") tagsbool[a] = true;
            }
        }

        private static void UpdatePIM(string theName, string dealerType)
        {
            while (String.Compare(p_sheet.Cells[p_r, p_namerow].Text, theName) < 0) p_r++;

            if (p_sheet.Cells[p_r, p_namerow].Text == theName)
            {

                while (p_sheet.Cells[p_r, p_namerow].Text == theName)
                {
                    for (var a = 0; a < tagsNo; a++)
                    {
                        if (tagsbool[a])
                        {
                            p_sheet.Cells[p_r, p_tagsstart + a].Value = tagsbool[a];
                        } else
                        {
                            p_sheet.Cells[p_r, p_tagsstart + a].Value = null;
                        }
                    }
                    if (!String.IsNullOrEmpty(dealerType)) p_sheet.Cells[p_r, p_dtype].Value = dealerType;
                    p_r++;
                }

            } else

            {
                p_infile.Save();
                Console.WriteLine("Didnt find " + theName);
                Console.WriteLine("Instead found: " + p_sheet.Cells[p_r, p_namerow].Text);
                Console.ReadLine();
                Environment.Exit(0);
            }

        }

        static void looptags()
        {
            string dealerType = "";

            Console.WriteLine("Opening Correcting Spreadsheet");
            var c_startRow = 2208; // Adjust these two to get all the rows. 
            var c_endRow = 2669;
            var c_path = "C:\\Mystuff\\PIM\\PIM Data Cleanup-V2.xlsx";
            var c_infile = new ExcelPackage(new FileInfo(c_path));
            var uc = 1;

            var c = FindWorksheet(c_infile, "Data");
            if (c > 0)
            {
                c_sheet = c_infile.Workbook.Worksheets[c];
            }
            else
            {
                Console.WriteLine("Couldnt find corrections Data worksheet");
                Console.ReadLine();
                Environment.Exit(0);
            }

            Console.WriteLine("Opening PIM Spreadsheet");
            var p_path = "C:\\Mystuff\\PIM\\PIM 19-07-17 AH.xlsx";
            p_infile = new ExcelPackage(new FileInfo(p_path));
            p_r = 4;

            var p = FindWorksheet(p_infile, "DATA Entry");

            if (p > 0)
            {
                p_sheet = p_infile.Workbook.Worksheets[p];
            }
            else
            {
                Console.WriteLine("Couldnt find PIM Data Entry worksheet");
                Console.ReadLine();
                Environment.Exit(0);
            }

            string pTitle = "";

            for (var c_r = c_startRow; c_r <= c_endRow; c_r++)

            {
                Console.WriteLine(c_sheet.Cells[c_r, 2].Text + " " + c_sheet.Cells[c_r, 5].Text);
                if (!((string)c_sheet.Cells[c_r, 5].Text == pTitle))
                {
                    // finish current product
                    if (!(c_r == c_startRow))
                    {
                        UpdatePIM(pTitle, dealerType);
                        if (uc % 50 == 0)
                        {
                            Console.WriteLine("Saving PIM ...");
                            p_infile.Save();
                            Console.ReadLine();

                        }
                        uc++;

                    }

                    // start new product
                    pTitle = c_sheet.Cells[c_r, 5].Text;
                    for (int a = 0; a < tagsNo; a++) tagsbool[a] = false;
                    dealerType = c_sheet.Cells[c_r, 51].Text;
                }

                checkTags(c_r);

            }

            p_infile.Save();

            Console.WriteLine("Completed Update");
            Console.ReadLine();
        }

    static void loopoptions()
        {
        

            Console.WriteLine("Opening Correcting Spreadsheet");
            var c_startRow = 7; // Adjust these two to get all the rows. 
            var c_endRow = 2669;
            var c_path = "C:\\Mystuff\\PIM\\PIM Data Cleanup-V2.xlsx";
            var c_infile = new ExcelPackage(new FileInfo(c_path));
            var uc = 1;

            var c = FindWorksheet(c_infile, "Data");
            if (c > 0)
            {
                c_sheet = c_infile.Workbook.Worksheets[c];
            }
            else
            {
                Console.WriteLine("Couldnt find corrections Data worksheet");
                Console.ReadLine();
                Environment.Exit(0);
            }

            Console.WriteLine("Opening PIM Spreadsheet");
            var p_path = "C:\\Mystuff\\PIM\\PIM 19-07-17 AH.xlsx";
            p_infile = new ExcelPackage(new FileInfo(p_path));
            p_r = 4;

            var p = FindWorksheet(p_infile, "DATA Entry");

            if (p > 0)
            {
                p_sheet = p_infile.Workbook.Worksheets[p];
            }
            else
            {
                Console.WriteLine("Couldnt find PIM Data Entry worksheet");
                Console.ReadLine();
                Environment.Exit(0);
            }

            uc = 0;

            for (var c_r = c_startRow; c_r <= c_endRow; c_r++)

            {
                Console.WriteLine(c_sheet.Cells[c_r, 2].Text + " " + c_sheet.Cells[c_r, 5].Text);

                var pr = 2;
                while(pr <= 3053)
                {
                    if (c_sheet.Cells[c_r, 2].Text == p_sheet.Cells[pr, 2].Text)
                    {
                        //found it so do we need to update
                        var b_update = false;
                        for (var a = 0; a <= 6; a++)
                            if (c_sheet.Cells[c_r, 10 + a].Text != p_sheet.Cells[pr, 108 + a].Text)
                            {
                                b_update = true;
                                break;
                            }

                        if (b_update)
                        {
                            Console.WriteLine(".... Updating PIM");
                            for (var a = 0; a <= 6; a++) p_sheet.Cells[pr, 108 + a].Value = c_sheet.Cells[c_r, 10 + a].Value;

                            if (uc % 100 == 0)
                            {
                                Console.WriteLine("Saving PIM ...");
                                p_infile.Save();
                                Console.ReadLine();

                            }
                            uc++;
                        }

                        break;
                    }

                    pr++;
                }
                
                if (pr>=3054)
                {
                    p_infile.Save();
                    Console.WriteLine(" .... Didnt find it ...");
                    
                    Console.ReadLine();
                    Environment.Exit(0);
                }
            }

            p_infile.Save();

            Console.WriteLine("Completed Update");
            Console.ReadLine();
        }

        


        static void Main(string[] args)
        {
            
            // looptags();
            loopoptions();

            

        }

    }
}
